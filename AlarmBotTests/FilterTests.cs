using AlarmBot.Data;
using AlarmBot.Filter;
using System;
using System.Collections.Generic;
using Xunit;

namespace AlarmBotTests
{
    public class FilterTests
    {
        [Fact]
        public void TestEmptyMessage()
        {
            EmpftyMessageFilter filter = new EmpftyMessageFilter();
            bool messageOK = filter.Excecute(new DefaultAlarmData(DateTime.Now, DateTime.Now, "123", "", ""), null);
            Assert.False(messageOK);
        }

        [Fact]
        public void TestNonEmptyMessage()
        {
            EmpftyMessageFilter filter = new EmpftyMessageFilter();
            bool messageOK = filter.Excecute(new DefaultAlarmData(DateTime.Now, DateTime.Now, "123", "Test", "Test"), null);
            Assert.True(messageOK);
        }

        [Fact]
        public void TestDublicateMessage()
        {
            DublicateMessageFilter filter = new DublicateMessageFilter();
            List<IAlarmData> data = new List<IAlarmData>();
            data.Add(new DefaultAlarmData(DateTime.Now, DateTime.Now, "123", "Test", "Test"));
            data.Add(new DefaultAlarmData(DateTime.Now.AddSeconds(5), DateTime.Now.AddSeconds(5), "123", "Test", "Test"));

            bool messageOK = filter.Excecute(new DefaultAlarmData(DateTime.Now.AddSeconds(10), DateTime.Now.AddSeconds(10), "123", "Test", "Test"), data);
            Assert.False(messageOK);
        }

        [Fact]
        public void TestNonDulicateMessage()
        {
            DublicateMessageFilter filter = new DublicateMessageFilter();
            List<IAlarmData> data = new List<IAlarmData>();

            bool messageOK = filter.Excecute(new DefaultAlarmData(DateTime.Now.AddSeconds(10), DateTime.Now.AddSeconds(10), "123", "Test", "Test"), data);
            Assert.True(messageOK);
        }
    }
}
