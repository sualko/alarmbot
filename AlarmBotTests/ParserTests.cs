﻿using AlarmBot.Data;
using AlarmBot.Parser.DrkBos;
using System;
using Xunit;

namespace AlarmBotTests
{
    public class ParserTests
    {
        [Fact]
        public void TestHvOParser()
        {
            DefaultAlarmData data = new DefaultAlarmData(DateTime.Now, DateTime.Now,
                "18B", "HvO Musterstatt",
                "Musterdorf, Musterstrasse 42 bei Mustermann, Erica" +
                "STIWO:RD Herzbeschwerden veränderte Hautfarbe " +
                "MSG:Hypertone Krise  jetzt ACS Symptomatik und starke Kopfschmerzen " +
                "ENR:123456789 HvO Musterstatt, RK 02/82-01");

            HvOParser parser = new HvOParser();
            IAlarmData newData = parser.Parse(data);
        }

    }
}
