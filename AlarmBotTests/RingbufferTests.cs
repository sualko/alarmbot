﻿using AlarmBot.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AlarmBotTests
{
    public class RingBufferTests
    {
        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        public void TestForBufferOverrun(int size)
        {
            RingBuffer<object> rb = new RingBuffer<object>(size);

            for (int i = 0; i < size+1; i++)
            {
                rb.Push(new object());
            }

            Assert.True(rb.Buffer.Count == size);
        }
    }
}
