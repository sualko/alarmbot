﻿using System;

namespace AlarmBot.Data
{
    public class RICData
    {
        public int MelderAdresse { get; set; }
        public int RIC { get; set; }

        public string AlarmtextA { get; set; }
        public string AlarmtextB { get; set; }
        public string AlarmtextC { get; set; }
        public string AlarmtextD { get; set; }

        public string ParserName { get; set; }

        /// <summary>
        /// Alarmtext des SubRics ausgeben
        /// </summary>
        /// <param name="subRic"></param>
        /// <returns></returns>
        public String GetSubRicText(string subRic)
        {
            switch (subRic.ToLower())
            {
                case "a":
                    return AlarmtextA;

                case "b":
                    return AlarmtextB;

                case "c":
                    return AlarmtextC;

                case "d":
                    return AlarmtextD;

                default:
                    return "Unbekannter subRic: " + subRic.ToLower();
            }
        }
    }
}
