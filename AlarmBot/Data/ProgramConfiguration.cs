﻿namespace AlarmBot.Data
{
    public class ProgramConfiguration
    {
        public string COMInterface { get; set; }
        public string ChatConfigPath { get; set; }
        public string RicConfigPath { get; set; }
        public string TelegramApiToken { get; set; }
    }
}
