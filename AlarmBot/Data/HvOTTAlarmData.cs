﻿using System.Text;

namespace AlarmBot.Data
{
    public class HvOTTAlarmData : DefaultAlarmData
    {
        public string Einsatzort { get; set; }
        public string Stichwort { get; set; }
        public string Meldungstext { get; set; }
        public string Einsatznummer { get; set; }

        public HvOTTAlarmData(IAlarmData defaultData, string einsatzort, string stichwort, string meldungstext, string einsatznummer) : 
            base(defaultData.Empfangszeit, defaultData.Alarmzeit, defaultData.Alarmadresse, defaultData.Alarmtext, defaultData.MeldungstextRaw)
        {
            Einsatzort = einsatzort;
            Stichwort = stichwort;
            Einsatznummer = einsatznummer;
            Meldungstext = meldungstext;
        }

        public override string ToLogString()
        {
            StringBuilder sb = new StringBuilder(base.ToLogString());
            sb.AppendLine("--- HvOTTAlarmData ---");
            sb.AppendLine($"Einstazort: {Einsatzort}");
            sb.AppendLine($"Stichwort: {Stichwort}");
            sb.AppendLine($"Meldungstext: {Meldungstext}");
            sb.AppendLine($"Einsaznummer: {Einsatznummer}");

            return sb.ToString();
        }
    }
}
