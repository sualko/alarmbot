﻿using AlarmBot.Communication.Messenger.Telegram;
using AlarmBot.Communication.Receiver;
using AlarmBot.Communication.Sender;
using AlarmBot.Data;
using AlarmBot.Helper;
using AlarmBot.Parser;
using AlarmBot.Persistence;
using NLog;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace AlarmBot
{
    class Program
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        private static readonly List<IAlarmReceiver> alarmReceiverList = new List<IAlarmReceiver>();
        private static readonly List<IAlarmDataWriter> writerList = new List<IAlarmDataWriter>();
        private static IAlarmMessenger alarmSender;
        private static ProgramConfiguration config;
        private static readonly AlarmFilter alarmFilter = new AlarmFilter();

        /// <summary>
        /// Programeinstiegspunkt
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            try
            {
                config = XMLHelper<ProgramConfiguration>.Deserialize("ProgramConfiguration.xml");

                RICDataReader.InitRicData(config.RicConfigPath);
                alarmSender = new TelegramMessenger(config.ChatConfigPath, config.TelegramApiToken);

                IAlarmDataWriter logWriter = AlarmDataWriterFactory.BuildAlarmWriter("logfile");
                writerList.Add(logWriter);

                IAlarmReceiver boss92xReceiver = AlarmReceiverFactory.BuildAlarmReceiver("boss92x", Receiver_AlarmReceived, Receiver_ErrorOccured, config);
                alarmReceiverList.Add(boss92xReceiver);

                //IAlarmReceiver smsReceiver = AlarmReceiverFactory.BuildAlarmReceiver("sms", Receiver_AlarmReceived, Receiver_ErrorOccured, config);
                //alarmReceiverList.Add(smsReceiver);

                string startText = $"AlarmBot Version {Assembly.GetExecutingAssembly().GetName().Version} gestartet und bereit auf {System.Net.Dns.GetHostName()}";

                await alarmSender.SendAdminMessage(startText);
                log.Info(startText);
                Console.WriteLine(startText);
            }
            catch (Exception e)
            {
                log.Error(e);
                await alarmSender.SendAdminMessage(e.Message);
                Environment.Exit(1);
            }

            while (true)
            {
                await Task.Delay(1000);
            }
        }

        /// <summary>
        /// Alle nicht abgefangenen Exceptions landen hier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            log.Fatal(ex, ex?.Message);
        }

        /// <summary>
        /// Fehler im SerialPort aufgetreten
        /// </summary>
        /// <param name="e"></param>
        private static async void Receiver_ErrorOccured(string message, Exception e)
        {
            log.Error(e, message);
            await alarmSender.SendAdminMessage($"{message} {e.Message}");
        }

        /// <summary>
        /// Alarm wurde vom Receiver empfangen
        /// </summary>
        /// <param name="data"></param>
        private static async void Receiver_AlarmReceived(IAlarmData data)
        {
            if (data == null)
            {
                await alarmSender.SendAdminMessage("Eingehender Alarm konnte nicht geparst werden.");
                return;
            }

            try
            {
                if (!alarmFilter.Excecute(data))
                {
                    // Alarm darf aufgrund eines Filterns nciht versendet werden
                    return;
                }

                data = ParserSelector.RunParser(RICDataReader.GetRicData(data.Alarmadresse).ParserName, data);

                await alarmSender?.SendAlarm(data);

                foreach (IAlarmDataWriter writer in writerList)
                {
                    writer.Write(data);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex, ex.Message);
                await alarmSender.SendAdminMessage(ex.Message);
            }
        }
    }
}
