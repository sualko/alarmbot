﻿using AlarmBot.Data;

namespace AlarmBot.Persistence
{
    public interface IAlarmDataWriter
    {
        /// <summary>
        /// Initialisieren der Klasse
        /// </summary>
        void Init();

        /// <summary>
        /// Alarmdata objekt wegschreiben
        /// </summary>
        /// <param name="data"></param>
        void Write(IAlarmData data);
    }
}
