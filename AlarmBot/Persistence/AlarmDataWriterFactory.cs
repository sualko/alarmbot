﻿using System;

namespace AlarmBot.Persistence
{
    /// <summary>
    /// Factorry zur erzeugung von AlarmDataWriter objekten
    /// </summary>
    public class AlarmDataWriterFactory
    {
        /// <summary>
        /// Spezifisches AlarmDataWriter Objekt erszeugen
        /// </summary>
        /// <param name="writerName"></param>
        /// <returns></returns>
        public static IAlarmDataWriter BuildAlarmWriter(string writerName)
        {
            switch (writerName.ToLower())
            {
                case "logfile":
                    return new LogFileWriter();

                default:
                    throw new NotImplementedException($"AlarmDataWriter '{writerName}' ist in dieser Version nicht implementiert.");
            }
        }
    }
}
