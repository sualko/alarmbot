﻿using System.Collections.Generic;
using AlarmBot.Data;
using NLog;

namespace AlarmBot.Filter
{
    /// <summary>
    /// Filter welcher auf leeren nachrichtentext prueft
    /// </summary>
    public class EmpftyMessageFilter : IAlarmFilter
    {
        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Filter ausfuehren
        /// </summary>
        /// <param name="data"></param>
        /// <param name="alarmBuffer"></param>
        /// <returns></returns>
        public bool Excecute(IAlarmData data, List<IAlarmData> alarmBuffer)
        {
            if (string.IsNullOrEmpty(data.MeldungstextRaw))
            {
                // Alarme ohne Text der Leitstelle werden nicht weitergeleitet. 
                // Dies sind meistens "oeffner" Alarme fuer den FME, welche faelschlich ueber die Schnittstelle kommen
                log.Info($"Alarm fuer Adresse {data.Alarmadresse} ohne Meldungstext empfangen. Verwerfe Alarmierung.");
                return false;
            }

            return true;
        }
    }
}
