﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlarmBot.Data;
using AlarmBot.Helper;
using NLog;

namespace AlarmBot.Filter
{
    /// <summary>
    /// Filter welcher mehrfache Alarmierungen ausfiltert
    /// </summary>
    public class DublicateMessageFilter : IAlarmFilter
    {
        private readonly ILogger log = LogManager.GetCurrentClassLogger();
        private const int FILTER_TIME = 30;

        /// <summary>
        /// Filter ausfuehren
        /// </summary>
        /// <param name="data"></param>
        /// <param name="alarmBuffer"></param>
        /// <returns>
        /// true == nachricht versand ok
        /// false == nachricht ausgefiltert
        /// </returns>
        public bool Excecute(IAlarmData data, List<IAlarmData> alarmBuffer)
        {

            IEnumerable<IAlarmData> selectedAlarms = alarmBuffer.Where(a => a.Alarmadresse == data.Alarmadresse);

            foreach (IAlarmData alarm in selectedAlarms)
            {
                // Gleiche Alarme innerhalb von "FILTER_TIME" ausfiltern
                log.Trace($"Check dublicate message:{Environment.NewLine}" +
                              $"lastAlarm Empfangszeit: {alarm?.Empfangszeit.ToLongTimeString()}{Environment.NewLine}" +
                              $"lastAlarm Empfangszeit (+{FILTER_TIME}s): {alarm?.Empfangszeit.AddSeconds(FILTER_TIME).ToLongTimeString()}{Environment.NewLine}" +
                              $"Time now: {DateTime.Now.ToLongTimeString()}");

                if (alarm?.Empfangszeit.AddSeconds(FILTER_TIME) > DateTime.Now)
                {
                    log.Info($"Alarm fuer Alarmadresse {data.Alarmadresse} {data.Alarmzeit} mehrfach empfangen. Alarmierung wird nicht versendet.");
                    //throw new AlarmFilterException("Doppelter Alarm empfangen");
                    return false;
                }
            }

            return true;
        }
    }
}
