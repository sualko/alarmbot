﻿using System.Text.RegularExpressions;
using AlarmBot.Data;
using NLog;

namespace AlarmBot.Parser.DrkBos
{
    public class HvOParser
    {
        private const string regexEinstazort = @".+(?=STIWO)";
        private const string regexSTIWO = @"STIWO:.+(?=MSG)";
        private const string regexMSG = @"MSG:.+(?=ENR)";
        private const string regexENR = @"ENR:.+";

        readonly Logger log = LogManager.GetCurrentClassLogger();

        public IAlarmData Parse(IAlarmData basicData)
        {
            Match einsatzortMatch = Regex.Match(basicData.MeldungstextRaw, regexEinstazort);
            Match stiwoMatch = Regex.Match(basicData.MeldungstextRaw, regexSTIWO);
            Match msgMatch = Regex.Match(basicData.MeldungstextRaw, regexMSG);
            Match enrMatch = Regex.Match(basicData.MeldungstextRaw, regexENR);

            IAlarmData extendetData = new HvOTTAlarmData(basicData, einsatzortMatch.Value.Trim(), stiwoMatch.Value.Trim(), msgMatch.Value.Trim(), enrMatch.Value.Trim());

            return extendetData;
        }
    }
}
