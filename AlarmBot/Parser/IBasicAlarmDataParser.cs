﻿using AlarmBot.Data;

namespace AlarmBot.Parser
{
    public interface IBasicAlarmDataParser
    {
        IAlarmData Parse(string rohdaten);
    }
}
