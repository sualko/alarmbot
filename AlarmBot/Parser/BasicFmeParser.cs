﻿using AlarmBot.Data;
using AlarmBot.Helper;
using NLog;
using System;

namespace AlarmBot.Parser
{
    /// <summary>
    /// Implementierung von <see cref="IBasicAlarmDataParser"/> welcher eine aktuell bekannte standard Nachricht vom BOS925 FME zerlegt
    /// </summary>
    public class BasicFmeParser : IBasicAlarmDataParser
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Rohdatenstring in <see cref="IAlarmData"/> object zerlegen
        /// </summary>
        /// <param name="rohdaten"></param>
        /// <returns></returns>
        public IAlarmData Parse(string rohdaten)
        {
            // tokens[0] = Alarmzeit
            // tokens[1] = Melderadresse
            // tokens[2] = Alarmtext
            string[] tokens = rohdaten.Split("\r\n", StringSplitOptions.RemoveEmptyEntries);
            if (tokens.Length < 2)
            {
                tokens = rohdaten.Split("\\r\\n", StringSplitOptions.RemoveEmptyEntries);
            }

            DateTime alarmzeit = DateTime.Now;
            string alarmadresse = "";
            string meldungstext = "";
            try
            {
                alarmzeit = DateTime.Parse(tokens[0]);
                alarmadresse = tokens[1];
                meldungstext = tokens[2];
            }
            catch (Exception e)
            {
                log.Error(e);
            }

            int.TryParse(alarmadresse.Substring(0, 2), out int melderAdresse);
            string subRic = alarmadresse.Substring(2, 1);

            RICData ricData = RICDataReader.GetRicData(melderAdresse);

            return new DefaultAlarmData(DateTime.Now, alarmzeit, alarmadresse, ricData.GetSubRicText(subRic), meldungstext);
        }
    }
}
