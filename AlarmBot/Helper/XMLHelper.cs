﻿using NLog;
using System;
using System.IO;
using System.Xml.Serialization;

namespace AlarmBot.Helper
{
    public class XMLHelper<T>
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Serializes n object to XML
        /// </summary>
        /// <param name="file"></param>
        /// <param name="t"></param>
        /// <param name="objectToSerialize"></param>
        public static void Serialize(string file, Type t, T objectToSerialize)
        {
            TextWriter writer = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(t);
                writer = new StreamWriter(file);
                serializer.Serialize(writer, objectToSerialize);
            }
            catch (Exception e)
            {
                log.Error(e);
            }
            finally
            {
                writer?.Flush();
                writer?.Close();
            }
        }

        /// <summary>
        /// Deserializes a XML File to an object
        /// </summary>
        /// <param name="file"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T Deserialize(string file)
        {
            TextReader reader = null;
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(T));
                reader = new StreamReader(file);
                object obj = deserializer.Deserialize(reader);
                return (T)obj;
            }
            catch (Exception e)
            {
                log.Error(e);
                throw;
            }
            finally
            {
                reader?.Close();
                reader = null;
            }
        }
    }
}
