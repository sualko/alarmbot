﻿using AlarmBot.Data;
using System.Collections.Generic;
using System.Linq;

namespace AlarmBot.Helper
{
    public class RICDataReader
    {
        private static List<RICData> rics;

        /// <summary>
        /// RicData XML File einlesen
        /// </summary>
        /// <param name="filePath"></param>
        public static void InitRicData(string filePath)
        {
            rics = XMLHelper<List<RICData>>.Deserialize(filePath);
        }

        /// <summary>
        /// RicData objekt anhand der Melderadresse ohne Subric abholen
        /// </summary>
        /// <param name="melderAdresse"></param>
        /// <returns></returns>
        public static RICData GetRicData(int melderAdresse)
        {
            return rics.Where(r => r.MelderAdresse == melderAdresse).First();
        }

        /// <summary>
        /// RicData objekt anhand der Melderadresse mit Subric (z.B. 15A) abholen
        /// </summary>
        /// <param name="melderadresseMitSubRic"></param>
        /// <returns></returns>
        public static RICData GetRicData(string melderadresseMitSubRic)
        {
            // erst mal sicherstellen das die Adresse immer 3 stellig ist
            melderadresseMitSubRic = melderadresseMitSubRic.PadLeft(3, '0');

            int.TryParse(melderadresseMitSubRic.Substring(0, 2), out int res);

            return rics.Where(r => r.MelderAdresse == res).First();
        }
    }
}
