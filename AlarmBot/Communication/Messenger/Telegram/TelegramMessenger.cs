﻿using AlarmBot.Communication.Sender;
using AlarmBot.Data;
using AlarmBot.Helper;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace AlarmBot.Communication.Messenger.Telegram
{
    /// <summary>
    /// Alarme an konfigurierte Telegram Gruppen senden
    /// </summary>
    public class TelegramMessenger : IAlarmMessenger
    {
        private static readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);
        private readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly TelegramBotClient bot;
        private readonly ReplyKeyboardMarkup replayKeyboard;
        private readonly List<Message> messagesToDelete = new List<Message>();
        private readonly Stopwatch sw = new Stopwatch();

        private readonly string chatConfigFile;
        private List<TelegramChatData> chatDataList;

        /// <summary>
        /// 
        /// </summary>
        public TelegramMessenger(string chatConfigFile, string apiToken)
        {
            bot = new TelegramBotClient(apiToken);
            this.chatConfigFile = chatConfigFile;

            List<KeyboardButton> keyboardButtons = new List<KeyboardButton>
            {
                new KeyboardButton("Ich komme"),
                //new KeyboardButton("Ich kann nicht")
            };
            replayKeyboard = new ReplyKeyboardMarkup(keyboardButtons)
            {
                OneTimeKeyboard = true,
            };

            chatDataList = XMLHelper<List<TelegramChatData>>.Deserialize(this.chatConfigFile);
            bot.OnMessage += Bot_OnMessage;
            bot.StartReceiving();

            Task.Run(DeleteOldMessages);
        }

        /// <summary>
        /// Nachrichten an den Bot empfangen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            log.Debug($"Received telegram message type {e.Message.Type.ToString()}");

            try
            {
                await ProcessMessageTypes(e.Message);
            }
            catch (AccessViolationException ex)
            {
                log.Error(ex);
                await SendAdminMessage(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                await bot.SendTextMessageAsync(new ChatId(e.Message.Chat.Id), "Unbekanntes Komando oder fehlende Berechtigung.");
            }
        }

        /// <summary>
        /// Telegram Nachrichtentypoen verarbeiten
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private async Task ProcessMessageTypes(Message message)
        {
            switch (message.Type)
            {
                case MessageType.Text:
                    if (!string.IsNullOrEmpty(message.Text) && message.Text.StartsWith("/"))
                    {
                        await ProcessBotCommands(message);
                    }
                    break;

                default:
                    return;
            }
        }

        /// <summary>
        /// Botkommandos behandeln
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private async Task ProcessBotCommands(Message message)
        {
            switch (message.Text.ToLower())
            {
                case "/getid":
                    log.Info($"User {message.From.FirstName} {message.From.LastName} asked for his ID {message.Chat.Id}");
                    await bot.SendTextMessageAsync(new ChatId(message.Chat.Id), $"Hallo {message.From.FirstName} {message.From.LastName} dies ist deine ChatID: " + message.Chat.Id);
                    break;

                case "/reloadchats":
                    CheckForAdminUser(message);
                    log.Info($"User {message.From.FirstName} {message.From.LastName} requested chatconfig reload");
                    chatDataList = XMLHelper<List<TelegramChatData>>.Deserialize(this.chatConfigFile);
                    log.Info($"Chatconfig reload finished.");
                    await SendAdminMessage("Chatreload successfull");
                    break;

                case "/version":
                    CheckForAdminUser(message);
                    await bot.SendTextMessageAsync(new ChatId(message.Chat.Id),
                        $"AlarmBot Version {Assembly.GetExecutingAssembly().GetName().Version} auf {System.Net.Dns.GetHostName()}");
                    break;

                default:
                    log.Info($"User {message.From.FirstName} {message.From.LastName} asked for unknown comand");
                    await bot.SendTextMessageAsync(new ChatId(message.Chat.Id), "Unbekanntes Komando oder fehlende Berechtigung.");
                    break;
            }
        }

        /// <summary>
        /// Pruefen ob der anfragende User admin ist
        /// </summary>
        /// <param name="chatMessage"></param>
        private void CheckForAdminUser(Message chatMessage)
        {
            TelegramChatData chatData = chatDataList.First(c => c.ChatId == chatMessage.Chat.Id);
            if (chatData != null && chatData.IsAdminChat)
            {
                return;
            }
            throw new AccessViolationException($"User {chatMessage.From.FirstName} {chatMessage.From.LastName} send admincomand {chatMessage.Text} but is no admin!");
        }

        /// <summary>
        /// Loeschen von Meldungsnachrichten nach definierter Zeit
        /// </summary>
        /// <returns></returns>
        private async Task DeleteOldMessages()
        {
            TimeSpan intervall = new TimeSpan(0, 1, 0);
            while (true)
            {
                List<Message> obsoleteMessages = new List<Message>();
                foreach (Message message in messagesToDelete)
                {
                    // vorhaltezeit fuer jeden chat aus config raussuchen
                    long chatId = message.Chat.Id;
                    int lifetime = chatDataList.Where(c => c.ChatId == chatId).First().DetailsLifetimeInMinutes;
                    DateTime now = DateTime.Now.AddMinutes(-lifetime);

                    log.Trace($"Check UTC intervall: Message send: {message.Date.ToUniversalTime().ToLongTimeString()} Time now ({-lifetime}): {now.ToUniversalTime().ToLongTimeString()}");
                    if (message.Date.ToUniversalTime() < now.ToUniversalTime())
                    {
                        try
                        {
                            await bot.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                        }
                        catch (Exception e)
                        {
                            log.Error(e, $"Exception beim Loeschen der Nachricht {message.MessageId} aus Chat {message.Chat.Id}");
                            continue;
                        }

                        log.Debug($"Nachricht {message.MessageId} gesendet um {message.Date.ToLongTimeString()} aus Chat {message.Chat.Id} geloescht.");

                        obsoleteMessages.Add(message);
                    }
                }

                foreach (Message message in obsoleteMessages)
                {
                    messagesToDelete.Remove(message);
                }

                await Task.Delay(intervall);
            }
        }

        /// <summary>
        /// Ein Alarmdata objekt versenden
        /// </summary>
        /// <param name="alarmData"></param>
        /// <returns></returns>
        public async Task SendAlarm(IAlarmData alarmData)
        {
            // Funktion locken, damit alarmtext und meldung immer als block gesendet werden.
            // bei gleichzeitigen Alarmen kann hier sonst was durcheinander kommen
            await semaphoreSlim.WaitAsync();

            try
            {
                int ric = RICDataReader.GetRicData(alarmData.Alarmadresse).RIC;
                IEnumerable<TelegramChatData> chatsToSend = chatDataList.Where((c) => c.RICs.Contains(ric));

                sw.Start();

                foreach (TelegramChatData chatData in chatsToSend)
                {
                    await SendTelegramGroupMessages(chatData, alarmData.ToMessageString(), alarmData.MeldungstextRaw);
                }
            }
            catch (Exception e)
            {
                log.Error(e, $"Exception beim Nachrichtenversand.");
            }
            finally
            {
                sw.Stop();
                log.Debug($"Zeit zum Senden der Nachrichten ueber Telegram: {sw.ElapsedMilliseconds}ms");
                sw.Reset();

                semaphoreSlim.Release();
            }
        }

        /// <summary>
        /// Alarmnachricht an Telegramm Gruppe senden
        /// </summary>
        /// <param name="chatData"></param>
        /// <returns></returns>
        private async Task SendTelegramGroupMessages(TelegramChatData chatData, string messageString, string meldungstext)
        {
            try
            {
                log.Info("Sende Alarmtext an Gruppe " + chatData.ChatName);

                ChatId chatId = new ChatId(chatData.ChatId);
                await bot.SendTextMessageAsync(chatId, messageString, replyMarkup: replayKeyboard);

                if (chatData.SendDetails)
                {
                    await SendAlarmDetails(chatData, meldungstext, chatId);
                }
            }
            catch (ApiRequestException e)
            {
                log.Error(e, $"Exception beim Nachrichtenversand an Gruppe {chatData.ChatName}.");
                await SendAdminMessage($"Exception beim Nachrichtenversand an Gruppe {chatData.ChatName}. {e.Message}");
            }
        }

        /// <summary>
        /// Leitstellennachricht verschicken
        /// </summary>
        /// <param name="chatData"></param>
        /// <param name="meldungstext"></param>
        /// <param name="chatId"></param>
        /// <returns></returns>
        private async Task SendAlarmDetails(TelegramChatData chatData, string meldungstext, ChatId chatId)
        {
            log.Info("Sende Alarmmeldung an Gruppe " + chatData.ChatName);

            Message detailMessage = await bot.SendTextMessageAsync(chatId, meldungstext);

            // nachrichten in admin chats oder bei lifetime == 0 werden nicht geloescht
            if (!chatData.IsAdminChat && chatData.DetailsLifetimeInMinutes > 0)
            {
                messagesToDelete.Add(detailMessage);
            }
        }

        /// <summary>
        /// Nachricht an alle Admin user senden
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendAdminMessage(string message)
        {
            IEnumerable<TelegramChatData> chatsToSend = chatDataList.Where((c) => c.IsAdminChat);

            foreach (TelegramChatData chatData in chatsToSend)
            {
                try
                {
                    await bot.SendTextMessageAsync(new ChatId(chatData.ChatId), message);
                }
                catch (Exception e)
                {
                    log.Error(e, $"Exception beim Nachrichtenversand an {chatData.ChatName}");
                }
            }
        }

        /// <summary>
        /// Freie Nachricht an definierte chatid senden
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendMessage(int chatId, string message)
        {
            try
            {
                await bot.SendTextMessageAsync(new ChatId(chatId), message);
            }
            catch (Exception e)
            {
                log.Error(e, $"Exception beim Nachrichtenversand an {chatId}");
            }
        }
    }
}
