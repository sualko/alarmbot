﻿using System.Collections.Generic;

namespace AlarmBot.Communication.Messenger.Telegram
{
    public class TelegramChatData
    {
        public string ChatName { get; set; }
        public long ChatId { get; set; }
        public List<int> RICs { get; set; }
        public bool SendDetails { get; set; }
        public int DetailsLifetimeInMinutes { get; set; }
        public bool IsAdminChat { get; set; }
    }
}
