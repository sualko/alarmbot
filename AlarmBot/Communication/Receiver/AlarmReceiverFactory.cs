﻿using AlarmBot.Data;
using AlarmBot.Parser;
using System;

namespace AlarmBot.Communication.Receiver
{
    /// <summary>
    /// Factory zur erzeugung der AlarmReceiver objekte
    /// </summary>
    public class AlarmReceiverFactory
    {
        /// <summary>
        /// Spezifisches Alarmreceiver Objekt erzeugen
        /// </summary>
        /// <param name="receiverName"></param>
        /// <param name="alarmReceived"></param>
        /// <param name="errorOccured"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IAlarmReceiver BuildAlarmReceiver(string receiverName, Action<IAlarmData> alarmReceived, Action<string, Exception> errorOccured, ProgramConfiguration config)
        {
            switch (receiverName.ToLower())
            {
                case "boss92x":
                    IAlarmReceiver alarmReceiver = BuildBoss92xReceiver(alarmReceived, errorOccured, config);
                    return alarmReceiver;

                case "sms":
                    throw new NotImplementedException($"SMS receiver  ist in dieser Version nicht implementiert");

                default:
                    throw new NotImplementedException($"Alarmreceiver '{receiverName}' ist in dieser Version nicht implementiert");
            }
        }

        /// <summary>
        /// Alarmreceiver fuer Swissphone FME BOSS92x erzeugen
        /// </summary>
        /// <param name="alarmReceived"></param>
        /// <param name="errorOccured"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        private static IAlarmReceiver BuildBoss92xReceiver(Action<IAlarmData> alarmReceived, Action<string, Exception> errorOccured, ProgramConfiguration config)
        {
            IAlarmReceiver alarmReceiver = new BOSS92xReceiver(config.COMInterface, new BasicFmeParser());
            alarmReceiver.AlarmReceived += alarmReceived;
            alarmReceiver.ErrorOccured += errorOccured;
            alarmReceiver.Connect();
            return alarmReceiver;
        }
    }
}
