﻿using AlarmBot.Data;
using AlarmBot.Parser;
using System;

namespace AlarmBot.Communication.Receiver
{
    public interface IAlarmReceiver
    {
        /// <summary>
        /// Verbindung zum empfangenden Geraet aufbauen
        /// </summary>
        void Connect();

        /// <summary>
        /// Fehler im Receiver aufgetreten
        /// </summary>
        event Action<string, Exception> ErrorOccured;

        /// <summary>
        /// Wird ausgeloest wenn ein neuer Alarm eingegangen ist
        /// </summary>
        event Action<IAlarmData> AlarmReceived;

        /// <summary>
        /// Parser um den empfangenen string in nutzdaten zu zerlegen
        /// </summary>
        IBasicAlarmDataParser DataParser { get; set; }
    }
}
