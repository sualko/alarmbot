﻿using AlarmBot.Data;
using AlarmBot.Parser;
using NLog;
using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading.Tasks;

namespace AlarmBot.Communication.Receiver
{
    /// <summary>
    /// Implementierung von <see cref="IAlarmReceiver"/> zum Datenempfang ueber einen 
    /// Swissphone BOSS920 bzw. BOSS925 Funkmeldeempfaenger
    /// </summary>
    public class BOSS92xReceiver : IAlarmReceiver, IDisposable
    {
        /// <summary>
        /// Konstantes Ende Zeichen der Meldungsausgabe
        /// Der BOSS92x sendet immer null-terminierte strings
        /// </summary>
        private const byte EOT = 0x00;

        private readonly Logger log = LogManager.GetCurrentClassLogger();
        private readonly Logger serialLog = LogManager.GetLogger("SerialLogger");
        private readonly Stopwatch sw = new Stopwatch();
        private readonly TimeSpan timeout = new TimeSpan(0, 0, 2);
        private readonly SerialPort serialPort;
        public IBasicAlarmDataParser DataParser { get; set; }
        public event Action<string, Exception> ErrorOccured;
        public event Action<IAlarmData> AlarmReceived;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comPort"></param>
        public BOSS92xReceiver(string comPort, IBasicAlarmDataParser parser)
        {
            serialPort = new SerialPort(comPort, 9600, Parity.None);
            serialPort.ErrorReceived += SerialPort_ErrorReceived;
            serialPort.DataReceived += SerialPort_DataReceived;

            DataParser = parser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            log.Info("Receiving serial data...");
            string serialData = "";
            bool isEot = false;

            sw.Start();

            while (!isEot)
            {
                if (sw.Elapsed > timeout)
                {
                    serialLog.Warn("Received invalid data in timeout 2s: " + Environment.NewLine + serialData);
                    serialPort.ReadExisting();
                    sw.Stop();
                    sw.Reset();
                    return;
                }

                while (serialPort.BytesToRead > 0)
                {
                    int dataByte = serialPort.ReadByte();

                    if (dataByte == EOT)
                    {
                        isEot = true;
                        break;
                    }
                    serialData += (char)dataByte;
                }
            }
            sw.Stop();
            log.Debug($"Zeit zum Empfang der Daten vom FME: {sw.ElapsedMilliseconds}ms");
            sw.Reset();

            Console.WriteLine(serialData);
            serialLog.Debug(serialData);

            IAlarmData alarmData;
            try
            {
                alarmData = DataParser?.Parse(serialData);
            }
            catch (Exception ex)
            {
                log.Error(e);
                ErrorOccured("Fehler beim Parsen der Alarmdaten", ex);
                return;
            }

            Task.Run(() => AlarmReceived?.Invoke(alarmData));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            serialLog.Error("Serial Error " + e.EventType.ToString());
            ErrorOccured?.Invoke("Fehler an der Seriellen Schnittstelle", new Exception(e.EventType.ToString()));
        }

        /// <summary>
        /// Verbindung zum empfaenger aufbauen
        /// </summary>
        public void Connect()
        {
            serialPort?.Open();
            log.Info($"Serialport {serialPort.PortName} open. Waiting for data.");
        }

        public void Dispose()
        {
            serialPort.Dispose();
        }
    }
}
