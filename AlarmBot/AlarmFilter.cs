﻿using AlarmBot.Data;
using AlarmBot.Filter;
using AlarmBot.Helper;
using NLog;
using System.Collections.Generic;

namespace AlarmBot
{
    /// <summary>
    /// 
    /// </summary>
    public class AlarmFilter
    {
        private readonly ILogger log = LogManager.GetCurrentClassLogger();
        private readonly RingBuffer<IAlarmData> alarmBuffer = new RingBuffer<IAlarmData>(10);
        private readonly List<IAlarmFilter> filterList = new List<IAlarmFilter>();

        /// <summary>
        /// 
        /// </summary>
        public AlarmFilter()
        {
            filterList.Add(new EmpftyMessageFilter());
            filterList.Add(new DublicateMessageFilter());
        }

        /// <summary>
        /// Alle Filter Ausfuehren
        /// </summary>
        /// <param name="data"></param>
        public bool Excecute(IAlarmData data)
        {
            foreach (IAlarmFilter filter in filterList)
            {
                if (!filter.Excecute(data, alarmBuffer.Buffer))
                {
                    return false;
                }
            }

            alarmBuffer.Push(data);
            return true;
        }
    }
}
