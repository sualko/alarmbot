# AlarmBot
AlarmBot was created as a second alarm way for the members of our German Red Cross organisation.

The idea was to create a simple system to introduce new members in the HvO (Hefler vor Ort) system to compensate the arrival time of the professional ambulance with trained helpers witch are located next to the emergency.

Now we receive the alarm from the rescue coordination center and send the important information via Telegram messenger to the configured usergroups.

In ouer organisation this application runs on a RaspberryPI with .NetCore3 with a attached Swissphone BOSS925 receiver.


## Installation
Install .NetCore3 on your RaspberryPi
*  download .NetCore3 You need the linux binarys for ARM32 for a raspberryPi <br/>https://dotnet.microsoft.com/download/dotnet-core/3.0<br/>
*  unpack the tar file to /opt/dotnet <br/> `sudo mkdir -p /opt/dotnet && sudo tar zxf dotnet.tar.gz -C /opt/dotnet`
*  set al linkt to dotnet in /usr/local/bin <br/> `sudo ln -s /opt/dotnet/dotnet /usr/local/bin`
*  check installation with command <br/> `dotnet --help`
*  copy the compiled sources to a folder of your choice
*  create the configuration files from the provided excamples and place them in the installation folder of AlarmBot
*  run it with `dontet ./AlarmBot.dll`

Actually there is no possibility to run it as a service. This has to be implemented in a further version. One possibility is run the programm inside a screen instance (see http://www.gnu.org/software/screen/ or https://help.ubuntu.com/community/Screen)

## Usage
1.  create a Telegram bot. See https://core.telegram.org/bots
2.  insert your Telegram-API key in the AlarmBot configuration file and start the software.
3.  create a Telegram group for your users and add the bot to the group
4.  to get the Telegram chatid you can ast the bot with the command `\getid` in the corresponding chatgroup
5.  now configure your chats in the ChatConfiguration.xml file
6.  the chatconfiguration can be reloaded with the command `\reloadchats`